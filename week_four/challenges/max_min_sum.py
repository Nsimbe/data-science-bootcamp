"""
Given five positive integers, find minimum and maximum values.
Calculate sum of exactly four of the five integers. 
Print the minimum sum and maximum sum of four of the five on a single line separated by space.
"""


def max_min_sum(arr: list[int]):
    total_sum = sum(arr)
    max_num = max(arr)
    min_num = min(arr)
    max_sum = total_sum - min_num
    min_sum = total_sum - max_num
    print(f"The maximum sum is:{max_sum}. The Minimum sum is {min_sum}")
    return max_sum, min_sum


if __name__ == "__main__":
    arr = [2, 5, 6, 8, 9]
    max_min_sum(arr)
