"""
String validation using methods .isalpha(), .isalnum(), .isdigit(), .islower() and .isupper().
First line print True if s is made of alphanumeric characters, otherwise print False 
Second line print True if s is made of the alphabet, otherwise print False 
Third line print True if s is made of digits, otherwise print False 
Forth line print True if s is lower case, otherwise print False 
Fifth line print True if s is upper case, otherwise print False 
"""
if __name__ == "__main__":
    s = input()
    is_alpha = True if s.isalpha() else False
    is_alnum = True if s.isalnum() else False
    is_digit = True if s.isdigit() else False
    is_lower = True if s.islower() else False
    is_upper = True if s.isupper() else False

    print(f"{is_alnum}\n{is_alpha}\n{is_digit}\n{is_lower}\n{is_upper}")
