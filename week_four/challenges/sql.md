# SQL challenge 
## SELECT statement

Query a table to return CITY names that do not end with a vowel

```sql
SELECT DISTINCT CITY FROM STATION WHERE CITY NOT LIKE '%[a,e,i,o,u,A,E,I,O,U]%';
```