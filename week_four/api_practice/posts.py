import requests


class Post:
    data = {"title": "My First Post", "body": "This is the content of my first post"}
    url = "https://jsonplaceholder.typicode.com/posts"

    @staticmethod
    def create_post():
        response = requests.post(Post.url, json=Post.data)
        created_post = response.json()
        print(f"Created post: {created_post}")
        print(response.status_code)
        return created_post

    @staticmethod
    def update_post():
        updates = {
            "title": "An update on Post",
            "body": "This is the content of the update on the post",
        }
        response = requests.put(
            "https://jsonplaceholder.typicode.com/posts/2",
            data=updates,
            headers={"Content-Type": "application-json"},
        )
        updated_post = response.json()
        print(f"Updated post: {updated_post}")
        print(response.status_code)
        return updated_post

    @staticmethod
    def update_part_of_post():
        post_id = Post.create_post()["id"]
        patched_data = {"title": "updated specific part of post"}
        response = requests.patch(f"{Post.url}/{post_id}", data=patched_data)
        print(response.status_code)
        return response.json()

    @staticmethod
    def filter_post():
        param = {"userId": 1, "id": 3}
        response = requests.get(Post.url, params=param)
        print(response.status_code)
        return response.json()

    @staticmethod
    def delete_post():
        response = requests.delete(
            url="https://jsonplaceholder.typicode.com/posts/userId=1"
        )
        print(response.status_code)
        return response.json()

    @staticmethod
    def get_comment():
        response = requests.get(f"{Post.url}/1/comments")
        print(response.status_code)
        return response.json()
