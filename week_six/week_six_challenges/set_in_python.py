# Python challenge using set()
# A set is an unordered collection of elements without duplicate entries
# Compute the average of all distinct heights in the array of heights
# The result should be rounded off to three decimal places
from statistics import mean

if __name__ == "__main__":
    arr = list(map(int, input().split()))
    print(round(mean(set(arr)), 3))
