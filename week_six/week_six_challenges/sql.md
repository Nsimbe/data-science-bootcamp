# SQL challenge
Write a query that prints a list of employee names (i.e. name attribute) for employees in Employee having a salary greater than 2000 per month who have been employees for less than 10 months.<br>
Sort your result by ascending employee_id.

```sql
Select name from Employee where (salary > 2000) and (month < 10) order by employee_id asc;
```