# Algorithm challenge
# Calculate the ratios of the elements of an array of integers that are positive, negative, and zero.
# Print the decimal value of each fraction.


def negative_positive_neutral_ratios(arr):
    array_size = len(arr)
    no_of_negatives = len(list(filter(lambda x: x < 0, arr)))
    no_of_positives = len(list(filter(lambda x: x > 0, arr)))
    no_of_neutrals = len(list(filter(lambda x: x == 0, arr)))

    return (
        no_of_negatives / array_size,
        no_of_neutrals / array_size,
        no_of_positives / array_size,
    )


if __name__ == "__main__":
    n = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    print(negative_positive_neutral_ratios(arr))
