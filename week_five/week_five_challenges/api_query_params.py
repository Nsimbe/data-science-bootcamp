import requests
import json


class JsonApi:
    @staticmethod
    def get_photos_by_album_id(albumId):
        response = requests.get(
            f"https://jsonplaceholder.typicode.com/photos?albumId={albumId}"
        )
        print(response.status_code)
        return response.json()

    @staticmethod
    def get_todo_by_id(id):
        response = requests.get(f"https://jsonplaceholder.typicode.com/todos?id={id}")
        print(response.status_code)
        return response.json()[0]

    @staticmethod
    def get_user_by_email(email):
        response = requests.get(
            f"https://jsonplaceholder.typicode.com/users?email={email}"
        )
        print(response.status_code)
        return response.json()[0]

    @staticmethod
    def get_comments_by_email(email):
        response = requests.get(
            f"https://jsonplaceholder.typicode.com/comments?email={email}"
        )
        print(response.status_code)
        return response.json()

    @staticmethod
    def search_products(search_term):
        response = requests.get(
            f"https://dummyjson.com/products/search?q={search_term}"
        )
        print(response.status_code)
        return response.json()

    @staticmethod
    def get_products(limit):
        products = []
        skip = 0

        while len(products) < 80:
            response = requests.get(
                f"https://dummyjson.com/products?limit={limit}&skip={skip}"
            )
            products_response = response.json()["products"]
            products.extend(products_response)
            skip = len(products)

        with open("product.json", "w") as f:
            json.dump(products, f)

        return products

    @staticmethod
    def get_products_by_category(category):
        response = requests.get(f"https://dummyjson.com/products/category/{category}")
        print(response.status_code)
        return response.json()

    @staticmethod
    def save_post_in_json_file():
        url = "https://jsonplaceholder.typicode.com/posts"
        post = {
            "title": "add post with json body",
            "body": "New post created with jsonplaceholder API",
        }
        header = {"content-Type": "application/json"}
        data = json.dumps(post)
        response = requests.post(url, headers=header, data=data)
        return response.status_code

    @staticmethod
    def submit_form_data():
        form_data = {"name": "ali"}
        file = {"resume": open("helo.json", "rb")}
        url = "https://postman-echo.com/post"
        response = requests.post(
            url,
            data=form_data,
            files=file,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )
        print(response.headers.get("Content-Type"))
        print(response.json())
        return response.status_code

    @staticmethod
    def submit_text():
        response = requests.post(
            "https://jsonplaceholder.typicode.com/posts",
            data="Hello Joanita",
            headers={"content-type": "text/plain"},
        )
        print(response.status_code)
        return response.text
