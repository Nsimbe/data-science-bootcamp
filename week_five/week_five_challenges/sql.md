# SQL challenge 
## Select statement
Query Name of student in STUDENTS who scored higher than 75 Marks.<br> 
Order output by the last three characters of each name.<br> 
If two or more students both have names ending in the same last three characters (i.e. Bobby, Robby) secondary sort by ascending ID.

```sql
SELECT Name FROM STUDENTS WHERE Marks > 75 ORDER BY SUBSTR(Name, -3), ID ASC;
```