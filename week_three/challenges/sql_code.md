# **SQL challenge**

## **Select by ID**
Query all columns in the table CITY with the ID 1661.

```sql
SELECT * FROM CITY WHERE ID = 1661;
```

## **Select statement**
Retrieve the ID, NAME, COUNTRYCODE, DISTRICT and POPULATION from the table CITY.

```sql
SELECT ID, NAME, COUNTRYCODE, DISTRICT, POPULATION FROM CITY;
```

