""" 
Data structures challenge with python.
Reverse Arrays!
The function takes a list of items in the place of argument 'a'.
Reverses the order of the items in the list.
Returns a new list with the items reversed.
"""

if __name__ == '__main__':
    
    def reverseArray(a):
        return a[::-1]
    
    a = [2, 4, 6, 8, 12, 24]
    print(reverseArray(a))