"""
Python challenge - For loop.
Loop through a range of a number.
The for loop prints the square of each number in the range of the number on a new line.
"""
if __name__ == "__main__":
    n = int(input("Enter a large number:"))
    for i in range(0, n):
        print(i**2)
