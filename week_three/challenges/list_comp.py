"""
Python challenge on list comprehension.
Use list comprehension to loop through x, y and z to create nested lists [x, y, z].
use constraint: x + y + z < n to loop through the range of x, y and z.
"""
if __name__ == "__main__":
    x = int(input("Enter first number:"))
    y = int(input("Enter second number:"))
    z = int(input("Enter third number:"))
    n = int(input("Enter the maximum limit:"))

    nested_list = [
        [i, j, k]
        for i in range(x + 1)
        for j in range(y + 1)
        for k in range(z + 1)
        if (i + j + k) != n
    ]

    print(nested_list)
