""" Use a function to calculate sum
The function that takes two integers 
and returns their sum.
"""
if __name__ == "__main__":

    def return_sum(num1, num2):
        return num1 + num2

    num1 = int(input("Enter first number:"))
    num2 = int(input("Enter second number:"))
    res = return_sum(num1, num2)
    print(res, "is the sum of the two numbers.")
