"""
Python challenge on IF-ELSE-IF statement.
Use if-else-if to check whether the number entered is weird or not weird based on the condition below.
>if number is odd, return weird.
>if number is even and in range of 2 to , return not weird.
>if number is even and in range 6 to 20, return weird.
>if number is even and greater than 20 return not weird
"""
if __name__ == "__main__":
    n = int(input('Enter a number of your choice:').strip())

    if n % 2 != 0 or n in range(6, 21):
        print("Weird")
    elif n in range(2, 6) or n > 20:
        print("Not Weird")
