"""
Python challenge on using split and join methods.
Create a function that takes a string in the place of 'line'.
The function splits the string at a space delimiter " ". Save the split string in a.
Use the join method to join the string using a hyphen delimiter "-". Save the joined string in b
Return b.
"""


def split_and_join(line):
    a = line.split(" ")
    b = "-".join(a)
    return b


if __name__ == "__main__":
    line = input("Enter a your first name and surname:")
    result = split_and_join(line)
    print(result)
